\documentclass[12pt]{article}

%\usepackage{savesym}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{enumerate}
%\pagenumbering{arabic}

\begin{document}

\centering \Large QuaLiKiz Saturation Rule

\vspace{0.2cm}
\small Summarised by Jonathan Citrin: updated 29.5.2019
\vspace{0.5cm}

\raggedright 

\section{Definitions}
GyroBohm diffusivity in $[m^2/s]$:
\begin{equation}
\chi_{GB}=\frac{m_i(1)^{0.5}T_e^{1.5}}{q_e^2B^2a_{LCFS}}
\end{equation}
Where $m_i(1)$ is the first ion in the QuaLiKiz ion input array. 

The reference Larmor radius is:
\begin{equation}
\rho_s = \frac{\sqrt{T_e m_i(1)}}{q_eB}
\end{equation}

\section{Quasilinear flux integrals}
Transport by fluctuations for species $s$ and wavenumber $k$ is defined as:
\begin{equation}
\Gamma_{s,k} = \langle \delta n_{s,k}\delta v_{r,k} \rangle_{t,\bf{x}}
\end{equation}
For particle transport, and:
\begin{equation}
Q_{s,k} = \langle \frac{3}{2} \delta P_{s,k}\delta v_{r,k} \rangle_{t,\bf{x}}
\end{equation}
For heat transport. The $E\times B$ radial velocity is $\delta v_{r,k}=\frac{ik_\theta}{B}\delta\phi_k$, where $\delta \phi_k$ is the fluctuating electrostatic potential. Assuming harmonic perturbations, and real (physical) quantities we have:
\begin{equation}
\delta \phi_k = (\delta \hat{\phi}_k) e^{-i(\bf{k\cdot x}+\omega t)}\ +\ c.c
\end{equation}
Where $\delta \hat{\phi}_k$ is a complex harmonic coefficient. Averaging over time, and taking the real part, we have, for example for particle transport:
\begin{equation}
\label{eq:flux}
\Gamma_{s,k} = -Im\langle \frac{k_\theta}{B}\delta \hat{n}_{s,k}\delta\hat{\phi}_k \rangle_{\bf{x}}
\end{equation}
The integration over $\bf{x}$ is a radial (in the local vicinity of a flux surface) and flux-surface averaging. This form is reminiscent of the Weak Form of the Poisson equation used in the dispersion relation solution: $\sum_s Z_s \langle\delta n_{s,k}\delta\phi^*_k \rangle_{\b{x}} =0$. Thus, the linear response from the dispersion relation can be applied in evaluating the moments for the perturbation of density, angular momentum, and pressure. The same integration routines can be applied. The evaluation of these integrals is taken, at each $k$, at the eigenvalue and eigenfunction approximated solutions found from the dispersion relation solution within QuaLiKiz. Specifically, we take the imaginary parts of the non-adiabatic part of these dispersion relation entities. Whereas in the dispersion relation these parts are summed together, the separated components are used for the quasilinear fluxes, and defined as the `quasilinear flux integrals'. We denote these as $L_{s,k,0}$ for the density moments, $L_{s,k,1}$ for the angular momentum moments, and $L_{s,k,2}$ for the energy moments. The definitions of the various $L$ terms includes the multiplication by $Z_s$ from quasineutrality. We thus need to divide by $Z_s$ for the flux determination.  

The total fluxes are, using the particle flux as an example:

\begin{equation}
\label{eq:flux2}
\Gamma_s = - \sum_k \frac{k_\theta}{Z_sB} |\delta \phi_k|^2 L_{s,k,0}
\end{equation}
Where $|\delta \phi_k|^2$ is the $k$-dependent amplitude of the saturated potential. Setting these values is the heart of the saturation rule, which approximates the non-linear physics. The components which comprise this saturation rule are laid out in the following sections.

\section{Determination of $|\phi_k|^2$}
A simple dimensional approximation for $\delta n_k$ is:
\begin{equation}
\delta n_{s,k} \propto \delta\phi_k \frac{q_e}{T_e} n_s
\end{equation}
We rewrite equation~\ref{eq:flux}, per wavenumber $k$, as follows [Bourdelle PoP 2007]: 
\begin{equation}
D_{QL} \equiv \frac{a\Gamma_{s,k}}{n_s}=\frac{R_0}{n_s}\frac{k_\theta}{B}\frac{n_sq_e}{T_e}|\phi_k|^2
\end{equation}
Where $a$ is a length scale, taken here as the minor radius. Invoking the mixing length rule, we set:
\begin{equation}
D_{QL} = C_{NL}\frac{\gamma_k}{k^2_\perp}|_{max} 
\end{equation}
Where $|_{max}$ means that we set this approximated diffusivity at the maximum of this term throughout the growth rate spectrum. $C_{NL}$ is a proportionality factor related to the nonlinear physics. 

We then obtain:
\begin{equation}
|\delta \phi_k|^2 = C_{NL}S_k\frac{\gamma_k}{k^2_\perp}|_{max} \frac{B}{k_{\theta,max} R_0}\frac{T_e}{q_e} 
\end{equation}
Where we have also introduced an additional form factor $S_k$ to set the functional $k$-dependence. Inserting into equation~\ref{eq:flux2}, we obtain:
\begin{equation}
\Gamma_s = - \sum_k \frac{1}{Z_s} C_{NL}S_k\frac{\gamma_k}{k^2_\perp}|_{max} \frac{k_\theta}{k_{\theta,max}}\frac{1}{R_0} \frac{T_e}{q_e} L_{s,k,0}
\end{equation}
Note that in QuaLiKiz itself, the $L_{s,k}$ terms are already normalised by $T_e/q_e$. We denote this normalised version of the flux integrals as $\tilde{L}_{s,k}$ and rewrite for simplicity:
\begin{equation}
\Gamma_s = - \sum_k \frac{1}{Z_s} C_{NL}S_k\frac{\gamma_k}{k^2_\perp}|_{max} \frac{k_\theta}{k_{\theta,max}}\frac{1}{R_0} \tilde{L}_{s,k,0}
\end{equation}

We now generalise to cases with more than one eigenvalue per wavenumber, denoted by eigenvalue $j$. In practice not more than 2 eigenvalues are resolved by QuaLiKiz at a given wavenumber. We note that the eigenvalues are ordered by magnitude (largest growth rate with $j=1$, etc.):

\begin{equation}
\Gamma_s = - \sum_{k,j} \frac{1}{Z_s} C_{NL}S_kS_j\frac{\gamma_{kj}}{k^2_\perp}|_{max,j} \frac{k_\theta}{k_{\theta,max,j}}\frac{1}{R_0} \tilde{L}_{s,k,j,0}
\end{equation}
In this model, we have introduced an additional eigenvalue dependent factor $S_j$ (to be determined below). $k_{\theta_{max}}$ is determined from the maximum $\frac{\gamma_{k,j}}{k^2_\perp}$ over all $k$ and $j$. $|_{max,j}$ denotes that the maximum is determined for a given $j$. Note that $S_k$ is not $j$ dependent and will be determined below.

\section{Scalar satuaration rule prefactors}

The $C_{NL}$ prefactor in the saturation rule depends on whether $k$ is in ETG or ITG scales. ITG scales, defined by $k_\theta\rho_s<2$, are tuned to the GA-Standard nonlinear GENE ion heat flux:
\begin{equation}
C_{NL-ITG}=271/s_{fac}
\end{equation}
ETG scales, defined by $k_\theta\rho_s>2$, are tuned to a single-scale GENE nonlinear run based on JET parameters [Citrin PPCF 2017], including a rudimentary multi-scale rule:
\begin{equation}
C_{NL-ETG} = 122\cdot f_{multi-scale} / s_{fac}
\end{equation}
Where $f_{multi-scale}$ is determined from ITG to ETG growth rate ratios, from the maxima of the respective spectra.
\begin{equation}
f_{multi-scale}=\frac{1}{1+e^{-\frac{1}{5}\left(\frac{\gamma_{ETG-max}}{\gamma_{ITG-max}}-\sqrt{\frac{m_i(1)}{m_e}}\right)}}
\end{equation}
The sigmoid assures that only for ETG growth rates at least a mass ratio greater than the ITG growth rate, can non-negligible ETG fluxes arise.

An added ad-hoc prefactor is also in place for low magnetic-shear [Citrin PoP 2012]:
\begin{equation}
s_{fac}=
\begin{cases}
2.5(1-|\hat{s}|) & \text{if}\ |\hat{s}|<0.6 \\
1.0 & \text{if}\ |\hat{s}|>0.6
\end{cases}
\end{equation}

\section{$k_{\perp}$ calculation}

In the shifted Gaussian mode-structure ansatz, due to the equivalence between $k_x$ and $\theta/d$ (where $d$ is the distance between rational surfaces), then the $k_x$ (normalised) contribution from the modeshift (if there is rotation) is:
\begin{equation}
k_{x-shift} = \left(d\frac{ Im(m_s)}{Re(m^2_w)}\right)^2
\end{equation}
Where $m_s$ is the modeshift, $m_w$ the modewidth. This enters the mean $\theta^2$ of the eigenmode:
\begin{equation}
\langle\theta^2\rangle=\frac{d^2}{2Re(m_w^2)}+k_{x-shift}
\end{equation}

The $k^2_x$ contribution from magnetic-shear ($\hat{s}$) is then:
\begin{equation}
k_{x-shear}^2=k_\theta^2\hat{s}^2\langle\theta^2\rangle
\end{equation}
For the `non-linear' contribution to $k_x^2$, we have the following tuning coefficients [Citrin PoP 2012]:
$c_{faca}=0.4$, $c_{facb}=2.0$, $c_{facc}=1.5$, $c_{facd}=0.2$, $q_{fac}=0.5$. Which provide non-negligible contribution at low-$\hat{s}$:
\begin{equation}
k_{x-NL}=\left(c_{faca}e^{-c_{facb}|\hat{s}|}\cdot q^{-q_{fac}}+max(k_\theta\rho_s-c_{facd},0)\cdot c_{facc} \right)/\rho_s
\end{equation}
Finally, the $k^2_\perp$ definition is:
\begin{equation}
k^2_\perp=
\begin{cases}
k^2_\theta+\left(k_{x-NL}+k_{x-shear}\right)^2, & \text{if}\ k_\theta < k_{ETG} \\
2\cdot k^2_\theta, & \text{if $k_\theta > k_{ETG}$ (isotropisation)}
\end{cases}
\end{equation}

%why no tau for particle transport?

\section{Spectral form factors}
The spectral form factor $S_k$ is determined from nonlinear turbulence simulations validated by turbulence diagnostics [Casati PhD]

\begin{equation}
S_{k}=
\begin{cases}
\left(\frac{k_\theta}{k_{\theta,max}}\right)^{-3} & \text{if}\ k_\theta > k_{\theta,max} \\
\frac{k_\theta}{k_{\theta,max}} & \text{if}\ k_\theta \le k_{\theta,max}
\end{cases}
\end{equation}
In multi-scale runs, the form-factor for $k_\theta>k_{ETG}$ is calculated with all ITG scales set to zero, and for $k_\theta<k_{ETG}$ is calculated with all ETG scales set to zero. The different scales do not interact regarding setting their respective form factors. 

$S_j$ is simply set by the growth rate ratios compared to the maximum growth rate out of all $j$ at a given $k$:
\begin{equation}
S_j = \frac{\gamma_{j,k}}{\gamma_{max,k}}
\end{equation}

\section{Summary}
Putting this all together, we then obtain:
\begin{equation}
\Gamma_s,\Pi_s,Q_s = - \int_k\sum_{j} \frac{1}{Z_s} C_{NL}\left(\frac{k_\theta}{k_{\theta,max}}\right)^{\alpha_k}\frac{\gamma_{j,k}}{\gamma_{max,k}}\frac{\gamma_{kj}}{k^2_\perp}|_{max,j} \frac{k_\theta}{k_{\theta,max,j}}\frac{1}{R_0} \tilde{L}_{s,k,j,(0,1,2)}
\end{equation}
Where $\alpha_k=-3$ for $k_\theta>k_{\theta,max}$ and $\alpha_k=1$ for $k_\theta<k_{\theta,max}$. In this definition we implicitly insert a $T_s$ term into $\tilde{L}_{s,k,j,2}$, and a $m_iv_{th}R$ term into $\tilde{L}_{s,k,j,1}$.

\section{GyroBohm fluxes}
This formulation scales as GyroBohm. This means that for the same dimensionless set of parameters, the fluxes scale as:
\begin{equation} 
\Gamma \propto \chi_{GB}\frac{n}{a}=\frac{n_sm_i^{0.5}T_e^{1.5}}{q_e^2B^2a^2}
\end{equation}
\begin{equation}
Q \propto \chi_{GB}\frac{Tn}{a}=\frac{n_sm_i^{0.5}T_e^{2.5}}{q_e^2B^2a^2} 
\end{equation}
and 
\begin{equation}
\Pi_i \propto \chi_{GB}\frac{mv_{th}Rn}{a}=\frac{n_im_iT_e^{1.5}T_iR}{q_e^2B^2a^2}
\end{equation}
$\Pi_e$ is neglected in QuaLiKiz.
It is interesting to observe from where this arises in the saturation rule. The term $\frac{\gamma_k}{k^2_\perp}$ scales as $\frac{c_s}{a}\rho_s^2=\chi_{GB}$. The rest of the scaling arises directly from the $\frac{1}{R_0}\tilde{L}_{s,k,j,(0,1,2)}$ terms (and when scaling at constant $\epsilon\equiv a/R_0$). 

%\section*{References}

%\bibliographystyle{unsrt}
%\bibliography{rotationbib}


\end{document}
